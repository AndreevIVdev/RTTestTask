//
//  RTErroe.swift
//  RTTestTask
//
//  Created by Илья Андреев on 17.02.2022.
//

enum RTError: String, Error {

    case invalidURL = "This string creates invalid URL, try another one."
    case unableToComplete = "Unable to complete your request. Please check your internet connection."
    case invalidResponse = "Invalid response from the server. Please try again."
    case invalidData = "The data received from the server was invalid. Please try again."
}
