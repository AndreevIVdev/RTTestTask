//
//  LoadingViewContoller.swift
//  RTTestTask
//
//  Created by Илья Андреев on 17.02.2022.
//

import UIKit

class LoadingViewController: UIViewController {

    private let containerView: UIView = .init()
    private let activityIndicator: UIActivityIndicatorView = .init(
        frame: CGRect(
            x: 0,
            y: 0,
            width: 50,
            height: 50
        )
    )

    func showLoadingView() {
        view.addSubViews(containerView)
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.pinToEdges(of: view)
        containerView.backgroundColor = .white

        UIView.animate(withDuration: 0.75) {
            self.containerView.alpha = 0.8
        }

        containerView.addSubViews(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            activityIndicator.centerYAnchor.constraint(equalTo: containerView.centerYAnchor),
            activityIndicator.centerXAnchor.constraint(equalTo: containerView.centerXAnchor)
        ])
        activityIndicator.startAnimating()
    }

    func dismissLoadingView() {
        activityIndicator.stopAnimating()
        activityIndicator.removeFromSuperview()
        containerView.removeFromSuperview()
    }
}
