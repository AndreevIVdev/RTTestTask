//
//  FullScreenImageViewController.swift
//  RTTestTask
//
//  Created by Илья Андреев on 17.02.2022.
//

import UIKit

class FullScreenImageViewController: UIViewController {
    private let imageView: UIImageView = .init()
    private let dateLabel: UILabel = .init()
    private var pictureInfo: PictureInfo!
    
    init(pictureInfo: PictureInfo) {
        super .init(nibName: nil, bundle: nil)
        self.pictureInfo = pictureInfo
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureViewController()
        configureDateLabel()
        configureImageView()
    }

    private func configureViewController() {
        view.addSubViews(dateLabel, imageView)
        view.backgroundColor = .white
    }
    
    private func configureDateLabel() {
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            dateLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            dateLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
        dateLabel.text = "Downloaded at: " + pictureInfo.downloded.convertToSedondsMinutesDaysMonthYearFormat()
        dateLabel.sizeToFit()
    }

    private func configureImageView() {
        imageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: dateLabel.bottomAnchor),
            imageView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
        NetworkManager.shared.fetchData(
            from: pictureInfo.url
        ) { [weak self] data in
            guard let self = self,
                  let data = data
            else { return }
            DispatchQueue.main.async {
                self.imageView.image = UIImage(data: data)
            }
        }
        imageView.contentMode = .scaleAspectFit
    }
}
