//
//  ViewController.swift
//  RTTestTask
//
//  Created by Илья Андреев on 17.02.2022.
//

import UIKit

class GalleryViewController: LoadingViewController {
    private let tableView: UITableView = .init()
    private var imageURLs: [URL] = []
    private var pictures: [Picture] = []
    private var isFetching = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViewController()
        configureTableView()
        getImageURLs { [weak self] in
            guard let self = self else {
                return
            }
            if self.imageURLs.count < 10 {
                self.fetchImages(from: self.imageURLs) { [weak self] in
                    self?.tableView.reloadData()
                }
            } else {
                self.fetchImages(from: Array(self.imageURLs[0..<10])) { [weak self] in
                    self?.tableView.reloadData()
                }
            }
        }
    }
    private func fetchImages(from urls: [URL], completion: @escaping () -> Void) {
        isFetching = true
        showLoadingView()
        let group = DispatchGroup()
        for url in urls {
            group.enter()
            NetworkManager.shared.fetchData(from: url) { [weak self] data in
                guard let self = self,
                      let data = data else { return }
                DispatchQueue.main.async {
                    self.pictures.append(
                        Picture(
                            pictureInfo: PictureInfo(
                                downloded: Date(),
                                url: url),
                            data: data
                        )
                    )
                }
                group.leave()
            }
        }
        group.notify(queue: .main) { [weak self] in
            self?.isFetching = false
            self?.dismissLoadingView()
            completion()
        }
    }
    
    private func configureViewController() {
        title = "Gallery"
        view.backgroundColor = .white
        view.addSubViews(tableView)
    }
    
    private func configureTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(
            RTTableViewCell.self,
            forCellReuseIdentifier: RTTableViewCell.self.description()
        )
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    private func getImageURLs(completion: @escaping () -> Void) {
        showLoadingView()
        NetworkManager.shared.fetch(
            from: Links.oneHundredImages,
            expecting: [String].self
        ) { [weak self] result in
            guard let self = self else {
                return
            }
            DispatchQueue.main.async {
                self.dismissLoadingView()
                defer {
                    completion()
                }
                switch result {
                    
                case .success(let response):
                    self.imageURLs += response.compactMap { URL(string: $0) }
                    
                case .failure(let error):
                    self.showAlert(
                        title: "Something went wrong",
                        message: error.rawValue)
                }
            }
        }
    }
}

extension GalleryViewController: UITableViewDelegate {}

extension GalleryViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        pictures.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: RTTableViewCell.self.description(),
            for: indexPath
        ) as! RTTableViewCell
        cell.centeredImageView.image = UIImage(data: pictures[indexPath.row].data)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        200
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        let height = scrollView.frame.size.height
        
        if offsetY > contentHeight - height {
            guard !isFetching else { return }
            
            if pictures.count + 10 >= imageURLs.count {
                getImageURLs {}
            } else {
                fetchImages(from: Array(imageURLs[pictures.count..<pictures.count + 10])) { [weak self] in
                    self?.tableView.reloadData()
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        navigationController?.pushViewController(
            FullScreenImageViewController(
                pictureInfo: pictures[indexPath.row].pictureInfo),
            animated: true
        )
    }
}
