//
//  PictureModel.swift
//  RTTestTask
//
//  Created by Илья Андреев on 17.02.2022.
//

import Foundation

struct Picture {
    let pictureInfo: PictureInfo
    let data: Data
}

struct PictureInfo {
    let downloded: Date
    let url: URL
}
