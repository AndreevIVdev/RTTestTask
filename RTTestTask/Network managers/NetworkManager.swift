//
//  NetworkManager.swift
//  RTTestTask
//
//  Created by Илья Андреев on 17.02.2022.
//

import Foundation

final class NetworkManager {

    static let shared: NetworkManager = .init()

    private var cache: [URL: Data] = [:]

    private init() {}

    func fetch<T: Codable>(
        from url: URL,
        expecting: T.Type,
        completed: @escaping (Result<T, RTError>) -> Void
    ) {
            URLSession.shared.dataTask(with: url) { data, response, error in

                if error != nil {
                    completed(.failure(.unableToComplete))
                }

                guard let response = response as? HTTPURLResponse,
                      response.statusCode == 200 else {
                          completed(.failure(.invalidResponse))
                          return
                      }

                guard let data = data else {
                    completed(.failure(.invalidData))
                    return
                }

                do {
                    let decoder = JSONDecoder()
                    let result = try decoder.decode(T.self, from: data)
                    completed(.success(result))
                } catch {
                    completed(.failure(.invalidData))
                }
            }
            .resume()
    }

    func fetchData(
        from url: URL,
        completed: @escaping (Data?) -> Void
    ) {
        if let data = cache[url] {
            completed(data)
            return
        }

        URLSession.shared.dataTask(with: url) { data, response, error in
            guard error == nil,
                  let response = response as? HTTPURLResponse, response.statusCode == 200,
                  let data = data
            else {
                completed(nil)
                return
            }
            // Синглтон проинициализируется
            // при первом обращении и будет
            // существовать весь жизненный цикл. Здесь не будет утечки.
            self.cache[url] = data
            completed(data)
        }
        .resume()
    }
}
