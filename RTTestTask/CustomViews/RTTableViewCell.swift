//
//  RTTableViewCell.swift
//  RTTestTask
//
//  Created by Илья Андреев on 17.02.2022.
//

import UIKit

class RTTableViewCell: UITableViewCell {
    
    let centeredImageView: UIImageView = .init()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubview(centeredImageView)
        centeredImageView.contentMode = .scaleAspectFit
        let padding: CGFloat = 12
        centeredImageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            centeredImageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            centeredImageView.centerXAnchor.constraint(equalTo: centerXAnchor),
            centeredImageView.heightAnchor.constraint(equalTo: heightAnchor, constant: -padding),
            centeredImageView.widthAnchor.constraint(equalTo: widthAnchor, constant: -padding)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
