//
//  URL+Ext.swift
//  RTTestTask
//
//  Created by Илья Андреев on 17.02.2022.
//

import Foundation

extension URL: ExpressibleByStringLiteral {
    
    public init(stringLiteral value: StaticString) {
        self.init(string: "\(value)")!
    }
}
