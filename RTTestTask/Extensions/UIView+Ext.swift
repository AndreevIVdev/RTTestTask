//
//  UIView+Ext.swift
//
//
//  Created by Илья Андреев on 01.11.2021.
//

import UIKit

extension UIView {

    func addSubViews(_ views: UIView...) {
        views.forEach {
            self.addSubview($0)
        }
    }

    func pinToEdges(of superView: UIView) {
        NSLayoutConstraint.activate([
            leadingAnchor.constraint(equalTo: superView.leadingAnchor),
            trailingAnchor.constraint(equalTo: superView.trailingAnchor),
            topAnchor.constraint(equalTo: superView.topAnchor),
            bottomAnchor.constraint(equalTo: superView.bottomAnchor)
        ])
    }
}
