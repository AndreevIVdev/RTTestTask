//
//  Date+Ext.swift
//  RTTestTask
//
//  Created by Илья Андреев on 17.02.2022.
//

import Foundation

extension Date {
    
    func convertToSedondsMinutesDaysMonthYearFormat() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "E, d MMM yyyy HH:mm:ss"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        return dateFormatter.string(from: self)
    }
}
